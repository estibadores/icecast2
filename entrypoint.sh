#!/bin/sh

set -e;

mkdir -p /icecast2/logs

cd /icecast2

[ -f icecast.xml ] || cp /etc/icecast.xml icecast.xml

xmlstarlet ed -L -u 'icecast/admin' -v $ICE_ADMIN_EMAIL icecast.xml
xmlstarlet ed -L -u 'icecast/limits/clients' -v $ICE_LIMITS_CLIENTS icecast.xml
xmlstarlet ed -L -u 'icecast/limits/sources' -v $ICE_LIMITS_SOURCES icecast.xml
xmlstarlet ed -L -u 'icecast/admin' -v $ICE_ADMIN_EMAIL icecast.xml
xmlstarlet ed -L -u 'icecast/authentication/source-password' -v $ICE_SOURCE_PASS icecast.xml
xmlstarlet ed -L -u 'icecast/authentication/admin-user' -v $ICE_ADMIN_USER icecast.xml
xmlstarlet ed -L -u 'icecast/authentication/admin-password' -v $ICE_ADMIN_PASS icecast.xml
xmlstarlet ed -L -u 'icecast/hostname' -v $ICE_HOSTNAME icecast.xml
xmlstarlet ed -L -u 'icecast/listen-socket/port' -v $ICE_PORT icecast.xml
xmlstarlet ed -L -u 'icecast/paths/basedir' -v $ICE_BASEDIR icecast.xml
xmlstarlet ed -L -u 'icecast/paths/logdir' -v $ICE_BASEDIR/logs icecast.xml
xmlstarlet ed -L -u 'icecast/paths/pidfile' -v $ICE_BASEDIR/icecast.pid icecast.xml

[ -n $ICE_SSL ] || xmlstarlet ed -L -i 'icecast/listen-socket/port' -t elem -n ssl -v 1 icecast.xml && xmlstarlet ed -L -d 'icecast/listen-socket/ssl' icecast.xml

exec $@
