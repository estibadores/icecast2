FROM registry.sindominio.net/debian

RUN apt-get update -y && apt-get full-upgrade -y \
	&& apt install -y icecast2 \
		ca-certificates xmlstarlet

WORKDIR icecast2

RUN cp /etc/icecast2/icecast.xml /tmp/icecast.xml \
	&& chmod 777 /tmp/icecast.xml

COPY entrypoint.sh /entrypoint.sh

ENTRYPOINT ["/bin/sh","/entrypoint.sh"]

CMD ["/usr/bin/icecast2","-c","/icecast2/icecast.xml"]

VOLUME /icecast2
