# Icecast 2 - Sindominio

## Preconfigure

Create volume directory:

```
mkdir data
```

Create custom .env file

```
cp .env.sample .env
vim .env
```

Change USER, PASSWORDS, LIMITS, ...
Dont change BASEDIR

## Build

```
docker-compose build
```

## Up

```
docker-compose up -d
```

## Logs

```
tail -f data/logs/*.log
```
